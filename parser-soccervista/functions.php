<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 20.12.2016
 * Time: 12:15
 */
function parse_soccervista_links(){
    include 'config.php';
    libxml_use_internal_errors(true);
    $c = curl_init($site_url);
    curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    $page = curl_exec($c);
    curl_close($c);
    $dom = new DOMDocument;
    $dom->loadHTML($page);
    $xpath = new DOMXpath($dom);
    $elements = $xpath->query(".//ul[@id='navlist2' and position()=1]/li[position()>1]/a");
    $href = array();
    if (!is_null($elements)) {
        foreach ($elements as $a){
            $name = $a->textContent;
            $name = preg_replace('/\s/', '', $name);
            $name = substr($name,1);
            $href[] = array('link' => $a->getAttribute('href'), 'name' => strtolower($name));
        }
    }
    $elements = $xpath->query(".//ul[@id='navlist2'and position()=2]/li[position()>1]/a");

    if (!is_null($elements)) {
        foreach ($elements as $a){
            $href[] = array('link' => $a->getAttribute('href'), 'name' => strtolower(trim($a->textContent)));
        }
    }

    $elements = $xpath->query(".//ul[@id='navlist2'and position()=3]/li[position()>1]/a");

    if (!is_null($elements)) {
        foreach ($elements as $a){
            $href[] = array('link' => $a->getAttribute('href'), 'name' => strtolower((trim($a->textContent))));
                    }
    }

    return $href;
}

function parse_soccervista_pages($url){
    include 'config.php';
    set_time_limit(360);
    libxml_use_internal_errors(true);
    $c = curl_init($site_url.$url);
    curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    $page = curl_exec($c);
    $httpCode = curl_getinfo($c, CURLINFO_HTTP_CODE);
    curl_close($c);

    $dom = new DOMDocument;
    if (!empty($page) || $httpCode != 404 ) {
        $dom->loadHTML($page);
        $xpath = new DOMXpath($dom);

        $result_arr = array();
        $table_num = 0;
        $tables = $xpath->query(".//table");
        $firstab = $xpath->query("tbody/tr[position()=1]/td", $tables->item(0));
        if ($firstab->length <= 3) {
            $table_num = 1;
        }

        $g = 0;
        if ($tables->length > 1) {
            if (!is_null($tables)) {
                if ($tables->item($table_num)->getAttribute('class') == 'upcoming') {
                    $elements = $xpath->query("tbody/tr", $tables->item($table_num));
                    if (!is_null($elements)) {
                        $td_flag = $xpath->query("td", $elements->item(0));
                        if ($td_flag->length == 11) {
                            foreach ($elements as $row) {
                                $td = $xpath->query("td", $row);
                                if ($td->length > 1) {
                                    $arr['date'] = $td->item(0)->textContent;
                                    $arr['round'] = $td->item(1)->textContent;
                                    //$arr['status'] = $td->item(2)->textContent;
                                    $arr['home'] = $td->item(2)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(2));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic1'] = $pic;
                                    $arr['away'] = $td->item(3)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(3));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic2'] = $pic;

                                    if ($td->item(4)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['rait1'] = $td->item(4)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['rait1'] = $td->item(4)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(5)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['raitx'] = $td->item(5)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['raitx'] = $td->item(5)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(6)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['rait2'] = $td->item(6)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['rait2'] = $td->item(6)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(7)->getElementsByTagName('i')->length != 0) {

                                        if ($td->item(7)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->length == 0) {
                                            $arr['respre'] = $td->item(7)->getElementsByTagName('i')->item(0)->textContent;
                                        } else {
                                            $arr['respre'] = $td->item(7)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                        }
                                    } else {
                                        var_dump($site_url . $url);
                                        var_dump($arr['home']);
                                        exit;
                                    }
                                    $arr['goals'] = $td->item(8)->textContent;
                                    $arr['tips'] = $td->item(9)->textContent;
                                    $result_arr['prediction1'][] = $arr;
                                    unset($arr);
                                }
                            }
                        } elseif ($td_flag->length == 10) {
                            $elements = $xpath->query("tbody/tr[position()>1]", $tables->item($table_num));
                            foreach ($elements as $row) {
                                $td = $xpath->query("td", $row);
                                if ($td->length > 1) {
                                    $arr['date'] = $td->item(0)->textContent;
                                    $arr['round'] = $td->item(1)->textContent;
                                    //$arr['status'] = $td->item(2)->textContent;
                                    $arr['home'] = $td->item(3)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(2));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic1'] = $pic;
                                    $arr['away'] = $td->item(5)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(4));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic2'] = $pic;

                                    if ($td->item(6)->getElementsByTagName('i')->length != 0) {
                                        if ($td->item(6)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->length == 0) {
                                            $arr['respre'] = $td->item(6)->getElementsByTagName('i')->item(0)->textContent;
                                        } else {
                                            $arr['respre'] = $td->item(6)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                        }
                                    } else {
                                        var_dump($site_url . $url);
                                        var_dump($arr['home']);
                                        exit;
                                    }
                                    $arr['goals'] = $td->item(7)->textContent;
                                    $arr['tips'] = $td->item(8)->textContent;
                                    $result_arr['prediction2'][] = $arr;
                                    unset($arr);
                                }
                            }
                        }
                    }
                    $table_num++;
                }

                if ($tables->item($table_num)->getAttribute('class') == 'upcoming') {
                    $elements = $xpath->query("tbody/tr", $tables->item($table_num));
                    if (!is_null($elements)) {
                        $td_flag = $xpath->query("td", $elements->item(0));
                        if ($td_flag->length == 11) {
                            foreach ($elements as $row) {
                                $td = $xpath->query("td", $row);
                                if ($td->length > 1) {
                                    $arr['date'] = $td->item(0)->textContent;
                                    $arr['round'] = $td->item(1)->textContent;
                                    //$arr['status'] = $td->item(2)->textContent;
                                    $arr['home'] = $td->item(2)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(2));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic1'] = $pic;
                                    $arr['away'] = $td->item(3)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(3));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic2'] = $pic;

                                    if ($td->item(4)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['rait1'] = $td->item(4)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['rait1'] = $td->item(4)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(5)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['raitx'] = $td->item(5)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['raitx'] = $td->item(5)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(6)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['rait2'] = $td->item(6)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['rait2'] = $td->item(6)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(7)->getElementsByTagName('i')->length != 0) {

                                        if ($td->item(7)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->length == 0) {
                                            $arr['respre'] = $td->item(7)->getElementsByTagName('i')->item(0)->textContent;
                                        } else {
                                            $arr['respre'] = $td->item(7)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                        }
                                    } else {
                                        var_dump($site_url . $url);
                                        var_dump($arr['home']);
                                        exit;
                                    }
                                    $arr['goals'] = $td->item(8)->textContent;
                                    $arr['tips'] = $td->item(9)->textContent;
                                    $result_arr['prediction1'][] = $arr;
                                    unset($arr);
                                }
                            }
                        } elseif ($td_flag->length == 10) {
                            $elements = $xpath->query("tbody/tr[position()>1]", $tables->item($table_num));
                            foreach ($elements as $row) {
                                $td = $xpath->query("td", $row);
                                if ($td->length > 1) {
                                    $arr['date'] = $td->item(0)->textContent;
                                    $arr['round'] = $td->item(1)->textContent;
                                    //$arr['status'] = $td->item(2)->textContent;
                                    $arr['home'] = $td->item(3)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(2));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic1'] = $pic;
                                    $arr['away'] = $td->item(5)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(4));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic2'] = $pic;

                                    if ($td->item(6)->getElementsByTagName('i')->length != 0) {
                                        if ($td->item(6)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->length == 0) {
                                            $arr['respre'] = $td->item(6)->getElementsByTagName('i')->item(0)->textContent;
                                        } else {
                                            $arr['respre'] = $td->item(6)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                        }
                                    } else {
                                        var_dump($site_url . $url);
                                        var_dump($arr['home']);
                                        exit;
                                    }
                                    $arr['goals'] = $td->item(7)->textContent;
                                    $arr['tips'] = $td->item(8)->textContent;
                                    $result_arr['prediction2'][] = $arr;
                                    unset($arr);
                                }
                            }
                        }
                    }
                    $table_num++;
                }

                if ($tables->item($table_num)->getAttribute('class') == 'upcoming') {
                    $elements = $xpath->query("tbody/tr", $tables->item($table_num));
                    if (!is_null($elements)) {
                        $td_flag = $xpath->query("td", $elements->item(0));
                        if ($td_flag->length == 11) {
                            foreach ($elements as $row) {
                                $td = $xpath->query("td", $row);
                                if ($td->length > 1) {
                                    $arr['date'] = $td->item(0)->textContent;
                                    $arr['round'] = $td->item(1)->textContent;
                                    //$arr['status'] = $td->item(2)->textContent;
                                    $arr['home'] = $td->item(2)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(2));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic1'] = $pic;
                                    $arr['away'] = $td->item(3)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(3));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic2'] = $pic;

                                    if ($td->item(4)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['rait1'] = $td->item(4)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['rait1'] = $td->item(4)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(5)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['raitx'] = $td->item(5)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['raitx'] = $td->item(5)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(6)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['rait2'] = $td->item(6)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['rait2'] = $td->item(6)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(7)->getElementsByTagName('i')->length != 0) {

                                        if ($td->item(7)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->length == 0) {
                                            $arr['respre'] = $td->item(7)->getElementsByTagName('i')->item(0)->textContent;
                                        } else {
                                            $arr['respre'] = $td->item(7)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                        }
                                    } else {
                                        var_dump($site_url . $url);
                                        var_dump($arr['home']);
                                        exit;
                                    }
                                    $arr['goals'] = $td->item(8)->textContent;
                                    $arr['tips'] = $td->item(9)->textContent;
                                    $result_arr['prediction1'][] = $arr;
                                    unset($arr);
                                }
                            }
                        } elseif ($td_flag->length == 10) {
                            $elements = $xpath->query("tbody/tr[position()>1]", $tables->item($table_num));
                            foreach ($elements as $row) {
                                $td = $xpath->query("td", $row);
                                if ($td->length > 1) {
                                    $arr['date'] = $td->item(0)->textContent;
                                    $arr['round'] = $td->item(1)->textContent;
                                    //$arr['status'] = $td->item(2)->textContent;
                                    $arr['home'] = $td->item(3)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(2));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic1'] = $pic;
                                    $arr['away'] = $td->item(5)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(4));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic2'] = $pic;

                                    if ($td->item(6)->getElementsByTagName('i')->length != 0) {
                                        if ($td->item(6)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->length == 0) {
                                            $arr['respre'] = $td->item(6)->getElementsByTagName('i')->item(0)->textContent;
                                        } else {
                                            $arr['respre'] = $td->item(6)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                        }
                                    } else {
                                        var_dump($site_url . $url);
                                        var_dump($arr['home']);
                                        exit;
                                    }
                                    $arr['goals'] = $td->item(7)->textContent;
                                    $arr['tips'] = $td->item(8)->textContent;
                                    $result_arr['prediction2'][] = $arr;
                                    unset($arr);
                                }
                            }
                        }
                    }
                    $table_num++;
                }

                if ($tables->item($table_num)->getAttribute('class') == 'upcoming') {
                    $elements = $xpath->query("tbody/tr", $tables->item($table_num));
                    if (!is_null($elements)) {
                        $td_flag = $xpath->query("td", $elements->item(0));
                        if ($td_flag->length == 11) {
                            foreach ($elements as $row) {
                                $td = $xpath->query("td", $row);
                                if ($td->length > 1) {
                                    $arr['date'] = $td->item(0)->textContent;
                                    $arr['round'] = $td->item(1)->textContent;
                                    //$arr['status'] = $td->item(2)->textContent;
                                    $arr['home'] = $td->item(2)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(2));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic1'] = $pic;
                                    $arr['away'] = $td->item(3)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(3));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic2'] = $pic;

                                    if ($td->item(4)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['rait1'] = $td->item(4)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['rait1'] = $td->item(4)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(5)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['raitx'] = $td->item(5)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['raitx'] = $td->item(5)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(6)->getElementsByTagName('a')->item(0)->textContent != '') {
                                        $arr['rait2'] = $td->item(6)->getElementsByTagName('a')->item(0)->textContent;
                                    } else {
                                        $arr['rait2'] = $td->item(6)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                    }
                                    if ($td->item(7)->getElementsByTagName('i')->length != 0) {

                                        if ($td->item(7)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->length == 0) {
                                            $arr['respre'] = $td->item(7)->getElementsByTagName('i')->item(0)->textContent;
                                        } else {
                                            $arr['respre'] = $td->item(7)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                        }
                                    } else {
                                        var_dump($site_url . $url);
                                        var_dump($arr['home']);
                                        exit;
                                    }
                                    $arr['goals'] = $td->item(8)->textContent;
                                    $arr['tips'] = $td->item(9)->textContent;
                                    $result_arr['prediction1'][] = $arr;
                                    unset($arr);
                                }
                            }
                        } elseif ($td_flag->length == 10) {
                            $elements = $xpath->query("tbody/tr[position()>1]", $tables->item($table_num));
                            foreach ($elements as $row) {
                                $td = $xpath->query("td", $row);
                                if ($td->length > 1) {
                                    $arr['date'] = $td->item(0)->textContent;
                                    $arr['round'] = $td->item(1)->textContent;
                                    //$arr['status'] = $td->item(2)->textContent;
                                    $arr['home'] = $td->item(3)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(2));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic1'] = $pic;
                                    $arr['away'] = $td->item(5)->getElementsByTagName('a')->item(0)->textContent;
                                    $span = $xpath->query("span", $td->item(4));
                                    $pic = array();
                                    foreach ($span as $s) {
                                        $pic[] = $s->getAttribute('class');
                                    }
                                    $arr['pic2'] = $pic;

                                    if ($td->item(6)->getElementsByTagName('i')->length != 0) {
                                        if ($td->item(6)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->length == 0) {
                                            $arr['respre'] = $td->item(6)->getElementsByTagName('i')->item(0)->textContent;
                                        } else {
                                            $arr['respre'] = $td->item(6)->getElementsByTagName('i')->item(0)->getElementsByTagName('font')->item(0)->textContent;
                                        }
                                    } else {
                                        var_dump($site_url . $url);
                                        var_dump($arr['home']);
                                        exit;
                                    }
                                    $arr['goals'] = $td->item(7)->textContent;
                                    $arr['tips'] = $td->item(8)->textContent;
                                    $result_arr['prediction2'][] = $arr;
                                    unset($arr);
                                }
                            }
                        }
                    }
                    $table_num++;
                }

//            if ($tables->item($table_num)->getAttribute('class') == 'upcoming') {
//                $elements = $xpath->query("tbody/tr", $tables->item($table_num));
//                if (!is_null($elements)) {
//                    foreach ($elements as $row) {
//                        $td = $xpath->query("td", $row);
//                        if ($td->length > 1) {
//                            $arr['date'] = $td->item(0)->textContent;
//                            $arr['round'] = $td->item(1)->textContent;
//                            //$arr['status'] = $td->item(2)->textContent;
//                            $arr['home'] = $td->item(3)->getElementsByTagName('a')->item(0)->textContent;
//                            $span = $xpath->query("span", $td->item(2));
//                            $pic = array();
//                            foreach ($span as $s) {
//                                $pic[] = $s->getAttribute('class');
//                            }
//                            $arr['pic1'] = $pic;
//                            $arr['away'] = $td->item(5)->getElementsByTagName('a')->item(0)->textContent;
//                            $span = $xpath->query("span", $td->item(4));
//                            $pic = array();
//                            foreach ($span as $s) {
//                                $pic[] = $s->getAttribute('class');
//                            }
//                            $arr['pic2'] = $pic;
//                            if ($td->item(6)->textContent != '') {
//                                $arr['respre'] = $td->item(6)->textContent;
//                            } else {
//                                $arr['respre'] = $td->item(6)->getElementsByTagName('font')->item(0)->textContent;
//                            }
//                            $arr['goals'] = $td->item(7)->textContent;
//                            $arr['tips'] = $td->item(8)->textContent;
//                            $result_arr['prediction2'][] = $arr;
//                            unset($arr);
//                        }
//                    }
//                }
//                $table_num++;
//            }
//
//            if ($tables->item($table_num)->getAttribute('class') == 'upcoming') {
//                $elements = $xpath->query("tbody/tr", $tables->item($table_num));
//                if (!is_null($elements)) {
//                    foreach ($elements as $row) {
//                        $td = $xpath->query("td", $row);
//                        if ($td->length > 1) {
//                            $arr['date'] = $td->item(0)->textContent;
//                            $arr['round'] = $td->item(1)->textContent;
//                            //$arr['status'] = $td->item(2)->textContent;
//                            $arr['home'] = $td->item(2)->getElementsByTagName('a')->item(0)->textContent;
//                            $span = $xpath->query("span", $td->item(2));
//                            $pic = array();
//                            foreach ($span as $s) {
//                                $pic[] = $s->getAttribute('class');
//                            }
//                            $arr['pic1'] = $pic;
//                            $arr['away'] = $td->item(3)->getElementsByTagName('a')->item(0)->textContent;
//                            $span = $xpath->query("span", $td->item(3));
//                            $pic = array();
//                            foreach ($span as $s) {
//                                $pic[] = $s->getAttribute('class');
//                            }
//                            $arr['pic2'] = $pic;
//
//                            if ($td->item(4)->getElementsByTagName('a')->item(0)->textContent != '') {
//                                $arr['rait1'] = $td->item(4)->getElementsByTagName('a')->item(0)->textContent;
//                            } else {
//                                $arr['rait1'] = $td->item(4)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
//                            }
//                            if ($td->item(5)->getElementsByTagName('a')->item(0)->textContent != '') {
//                                $arr['raitx'] = $td->item(5)->getElementsByTagName('a')->item(0)->textContent;
//                            } else {
//                                $arr['raitx'] = $td->item(5)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
//                            }
//                            if ($td->item(6)->getElementsByTagName('a')->item(0)->textContent != '') {
//                                $arr['rait2'] = $td->item(6)->getElementsByTagName('a')->item(0)->textContent;
//                            } else {
//                                $arr['rait2'] = $td->item(6)->getElementsByTagName('a')->item(0)->getElementsByTagName('font')->item(0)->textContent;
//                            }
//
//                            if ($td->item(7)->textContent != '') {
//                                $arr['respre'] = $td->item(7)->textContent;
//                            } else {
//                                $arr['respre'] = $td->item(7)->getElementsByTagName('font')->item(0)->textContent;
//                            }
//                            $arr['goals'] = $td->item(8)->textContent;
//                            $arr['tips'] = $td->item(9)->textContent;
//                            $result_arr['prediction1'][] = $arr;
//                            unset($arr);
//                        }
//                    }
//                }
//                $table_num++;
//            }


                $elements = $xpath->query("tbody/tr[position()<last()]", $tables->item($table_num));
                if (!is_null($elements)) {
                    foreach ($elements as $row) {
                        $td = $xpath->query("td", $row);
                        $arr['rank'] = $td->item(0)->textContent;
                        $arr['name'] = $td->item(1)->getElementsByTagName('a')->item(0)->textContent;
                        $arr['game'] = $td->item(2)->textContent;
                        $arr['win'] = $td->item(3)->textContent;
                        $arr['draw'] = $td->item(4)->textContent;
                        $arr['los'] = $td->item(5)->textContent;
                        $arr['gols'] = $td->item(6)->textContent;
                        $arr['points'] = $td->item(7)->getElementsByTagName('strong')->item(0)->textContent;
                        $result_arr['big'][] = $arr;
                        $g = $g + (int)$td->item(2)->textContent;
                        unset($arr);
                    }
                    $table_num++;
                }

//        $elements = $xpath->query("tbody/tr", $tables->item($table_num));
//        if (!is_null($elements)) {
//            foreach ($elements as $row){
//                $td = $xpath->query("td",$row);
//                $arr['rank'] = $td->item(0)->textContent;
//                $arr['name']= $td->item(1)->textContent;
//                $arr['game']= $td->item(2)->textContent;
//                $arr['win']= $td->item(3)->textContent;
//                $arr['draw']= $td->item(4)->textContent;
//                $arr['los']= $td->item(5)->textContent;
//                $arr['gols']= $td->item(6)->textContent;
//                $arr['points']= $td->item(7)->textContent;
//                $result_arr['homonly'][] = $arr;
//                unset($arr);
//            }
//            $table_num++;
//        }

                $elements = $xpath->query("tbody/tr", $tables->item($table_num));
                if (!is_null($elements)) {
                    foreach ($elements as $row) {
                        $td = $xpath->query("td", $row);
                        $arr['rank'] = $td->item(0)->textContent;
                        $arr['name'] = $td->item(1)->textContent;
                        $arr['game'] = $td->item(2)->textContent;
                        $arr['win'] = $td->item(3)->textContent;
                        $arr['draw'] = $td->item(4)->textContent;
                        $arr['los'] = $td->item(5)->textContent;
                        $arr['gols'] = $td->item(6)->textContent;
                        $arr['points'] = $td->item(7)->textContent;
                        $result_arr['homonly'][] = $arr;
                        unset($arr);
                    }
                    $table_num++;
                }

                $elements = $xpath->query("tbody/tr", $tables->item($table_num));
                if (!is_null($elements)) {
                    foreach ($elements as $row) {
                        $td = $xpath->query("td", $row);
                        $arr['rank'] = $td->item(0)->textContent;
                        $arr['name'] = $td->item(1)->textContent;
                        $arr['game'] = $td->item(2)->textContent;
                        $arr['win'] = $td->item(3)->textContent;
                        $arr['draw'] = $td->item(4)->textContent;
                        $arr['los'] = $td->item(5)->textContent;
                        $arr['gols'] = $td->item(6)->textContent;
                        $arr['points'] = $td->item(7)->textContent;
                        $result_arr['away'][] = $arr;
                        unset($arr);
                    }
                    $table_num++;
                }

                if ($g > 0) {
                    $elements = $xpath->query("tbody/tr", $tables->item($table_num));
                    if (!is_null($elements)) {
                        foreach ($elements as $row) {
                            $td = $xpath->query("td", $row);
                            if ($td->length > 1) {
                                $arr['date'] = $td->item(0)->textContent;
                                $arr['round'] = $td->item(1)->textContent;
                                $arr['status'] = $td->item(2)->textContent;
                                if ($td->item(3)->textContent != '') {
                                    $arr['home'] = $td->item(3)->textContent;
                                } else {
                                    $arr['home'] = $td->item(3)->getElementsByTagName('font')->item(0)->textContent;
                                }
                                $arr['scope'] = $td->item(4)->textContent;
                                if ($td->item(5)->textContent != '') {
                                    $arr['await'] = $td->item(5)->textContent;
                                } else {
                                    $arr['await'] = $td->item(5)->getElementsByTagName('font')->item(0)->textContent;
                                }
                                $result_arr['latest'][] = $arr;
                                unset($arr);
                            }
                        }
                        $table_num++;
                    }

                    $elements = $xpath->query("tbody/tr", $tables->item($table_num));
                    if (!is_null($elements)) {
                        $td_flag = $xpath->query("td", $elements->item(0));
                        if ($td_flag->length == 8) {
                            foreach ($elements as $row) {
                                $td = $xpath->query("td", $row);
                                $arr['rank'] = $td->item(0)->textContent;
                                $arr['name'] = $td->item(1)->textContent;
                                $arr['game'] = $td->item(2)->textContent;
                                $arr['win'] = $td->item(3)->textContent;
                                $arr['draw'] = $td->item(4)->textContent;
                                $arr['los'] = $td->item(5)->textContent;
                                $arr['gols'] = $td->item(6)->textContent;
                                $arr['points'] = $td->item(7)->textContent;
                                $result_arr['last5'][] = $arr;
                                unset($arr);
                            }
                            $table_num++;
                        }
                    }

                    $elements = $xpath->query("tbody/tr", $tables->item($table_num));
                    if (!is_null($elements)) {
                        $td_flag = $xpath->query("td", $elements->item(0));
                        if ($td_flag->length == 8) {
                            foreach ($elements as $row) {
                                $td = $xpath->query("td", $row);
                                $arr['rank'] = $td->item(0)->textContent;
                                $arr['name'] = $td->item(1)->textContent;
                                $arr['game'] = $td->item(2)->textContent;
                                $arr['win'] = $td->item(3)->textContent;
                                $arr['draw'] = $td->item(4)->textContent;
                                $arr['los'] = $td->item(5)->textContent;
                                $arr['gols'] = $td->item(6)->textContent;
                                $arr['points'] = $td->item(7)->textContent;
                                $result_arr['last5home'][] = $arr;
                                unset($arr);
                            }
                            $table_num++;
                        }
                    }

                    $elements = $xpath->query("tbody/tr", $tables->item($table_num));
                    if (!is_null($elements)) {
                        $td_flag = $xpath->query("td", $elements->item(0));
                        if ($td_flag->length == 8) {
                            foreach ($elements as $row) {
                                $td = $xpath->query("td", $row);
                                $arr['rank'] = $td->item(0)->textContent;
                                $arr['name'] = $td->item(1)->textContent;
                                $arr['game'] = $td->item(2)->textContent;
                                $arr['win'] = $td->item(3)->textContent;
                                $arr['draw'] = $td->item(4)->textContent;
                                $arr['los'] = $td->item(5)->textContent;
                                $arr['gols'] = $td->item(6)->textContent;
                                $arr['points'] = $td->item(7)->textContent;
                                $result_arr['last5away'][] = $arr;
                                unset($arr);
                            }
                            $table_num++;
                        }
                    }
                }
                $elements = $xpath->query("tbody/tr[position()>1]", $tables->item($table_num));
                if (!is_null($elements)) {
                    foreach ($elements as $row) {
                        $td = $xpath->query("td", $row);
                        $arr['rank'] = $td->item(0)->textContent;
                        $arr['name'] = $td->item(1)->textContent;
                        $arr['over15'] = $td->item(2)->textContent;
                        $arr['over25'] = $td->item(3)->textContent;
                        $arr['over35'] = $td->item(4)->textContent;
                        $arr['averegehome'] = $td->item(5)->textContent;
                        $arr['averegeaway'] = $td->item(6)->textContent;
                        $arr['averegetotal'] = $td->item(7)->textContent;
                        $result_arr['statistic'][] = $arr;
                        unset($arr);
                    }
                }

            }
        } elseif ($tables->length == 1) {
            $elements = $xpath->query("tbody/tr", $tables->item($table_num));
            if (!is_null($elements)) {
                foreach ($elements as $row) {
                    $td = $xpath->query("td", $row);
                    if ($td->length > 1) {
                        $arr['date'] = $td->item(0)->textContent;
                        $arr['round'] = $td->item(1)->textContent;
                        $arr['status'] = $td->item(2)->textContent;
                        if ($td->item(3)->textContent != '') {
                            $arr['home'] = $td->item(3)->textContent;
                        } else {
                            $arr['home'] = $td->item(3)->getElementsByTagName('font')->item(0)->textContent;
                        }
                        $arr['scope'] = $td->item(4)->textContent;
                        if ($td->item(5)->textContent != '') {
                            $arr['await'] = $td->item(5)->textContent;
                        } else {
                            $arr['await'] = $td->item(5)->getElementsByTagName('font')->item(0)->textContent;
                        }
                        $result_arr['latest'][] = $arr;
                        unset($arr);
                    }
                }
                $table_num++;
            }
        }
        return $result_arr;
    }
    else{
        echo "Error page:" . $site_url.$url;
        echo "HTTP CODE:" . $httpCode;
    }
}

function parse_soccervista_main_page(){
    include 'config.php';
    libxml_use_internal_errors(true);
    $c = curl_init($site_url);

    curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    $page = curl_exec($c);
    curl_close($c);

    $dom = new DOMDocument;
    $dom->loadHTML($page);

    $xpath = new DOMXpath($dom);

    $elements = $xpath->query(".//table[@class='main']/tr[position()>0]");
    $result_arr = array();
    if (!is_null($elements)) {
        foreach ($elements as $row){
            $td = $xpath->query("td",$row);

            if ($row->getAttribute('class') == 'headupe'){
                $coutry = $td->item(1)->textContent;
                $liga = $td->item(2)->getElementsByTagName('a')->item(0)->textContent;
            }
            elseif ($row->getAttribute('class') == 'onem' OR $row->getAttribute('class') == 'twom' ){
                $arr['country'] = $coutry;
                $arr['liga'] = $liga;
                $arr['time'] = $td->item(0)->textContent;
                $span = $xpath->query("span",$td->item(1));
                $pic = array();
                foreach ($span as $s){
                    $pic[] = $s->getAttribute('class');
                }
                $arr['pic1'] = $pic;
                $arr['home']= $td->item(2)->textContent;
                $arr['scope']= $td->item(3)->getElementsByTagName('a')->item(0)->textContent;
                $arr['away']= $td->item(4)->textContent;
                $span = $xpath->query("span",$td->item(5));
                $pic = array();
                foreach ($span as $s){
                    $pic[] = $s->getAttribute('class');
                }
                $arr['pic2'] = $pic;
                $arr['odds1']= $td->item(6)->textContent;
                $arr['odds2']= $td->item(7)->textContent;
                $arr['odds3']= $td->item(8)->textContent;
                $arr['tips1']= $td->item(9)->getElementsByTagName('strong')->item(0)->textContent;
                $arr['tips2']= $td->item(10)->textContent;
                $arr['tips3']= $td->item(11)->textContent;
                $result_arr[]=$arr;
            }
        }
    }

    return $result_arr;
}

function parse_soccervista_activation() {
    parser_soccervista_create_table();
    parser_soccervista_add_to_db_main_table();
    parser_soccervista_add_to_db_country_page();
    parser_soccervista_add_to_db_country_table();
}

function parse_soccervista_deactivation() {
    parser_soccervista_unset_table();
}

function parser_soccervista_create_table(){
    global $wpdb;
    $table_name = $wpdb->prefix.'parser_soccervista_country';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, name VARCHAR(50), link VARCHAR(50),PRIMARY KEY id(id))";
        $wpdb->query($sql);

    }
    $table_name = $wpdb->prefix.'parser_soccervista_main_table';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, country VARCHAR(100),  liga VARCHAR(100), time VARCHAR(20), pic1 VARCHAR(100), pic2 VARCHAR(100), home VARCHAR(100), away VARCHAR(100), scope VARCHAR(100),
         odds1 VARCHAR(100), odds2 VARCHAR(100), odds3 VARCHAR(100), tips1 VARCHAR(100), tips2 VARCHAR(100), tips3 VARCHAR(100), PRIMARY KEY id(id))";
        $wpdb->query($sql);
    }

    $table_name = $wpdb->prefix.'parser_soccervista_prediction1';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, country VARCHAR(100),  date VARCHAR(100), round VARCHAR(20), pic1 VARCHAR(100), pic2 VARCHAR(100), home VARCHAR(100), away VARCHAR(100), rait1 VARCHAR(100),
         raitx VARCHAR(100), rait2 VARCHAR(100), respre VARCHAR(100), goals VARCHAR(100), tips VARCHAR(100), PRIMARY KEY id(id))";
        $wpdb->query($sql);
    }

    $table_name = $wpdb->prefix.'parser_soccervista_prediction2';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, country VARCHAR(100),  date VARCHAR(100), round VARCHAR(20), pic1 VARCHAR(100), pic2 VARCHAR(100), home VARCHAR(100), away VARCHAR(100),
         respre VARCHAR(100), goals VARCHAR(100), tips VARCHAR(100), PRIMARY KEY id(id))";
        $wpdb->query($sql);
    }

    $table_name = $wpdb->prefix.'parser_soccervista_big';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, country VARCHAR(100),  rank VARCHAR(100), name VARCHAR(20), game VARCHAR(100), win VARCHAR(100), draw VARCHAR(100), los VARCHAR(100),
        goals VARCHAR(100), points VARCHAR(100), PRIMARY KEY id(id))";
        $wpdb->query($sql);
    }

    $table_name = $wpdb->prefix.'parser_soccervista_homonly';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, country VARCHAR(100),  rank VARCHAR(100), name VARCHAR(20), game VARCHAR(100), win VARCHAR(100), draw VARCHAR(100), los VARCHAR(100),
        goals VARCHAR(100), points VARCHAR(100), PRIMARY KEY id(id))";
        $wpdb->query($sql);
    }
    $table_name = $wpdb->prefix.'parser_soccervista_away';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, country VARCHAR(100),  rank VARCHAR(100), name VARCHAR(20), game VARCHAR(100), win VARCHAR(100), draw VARCHAR(100), los VARCHAR(100),
        goals VARCHAR(100), points VARCHAR(100), PRIMARY KEY id(id))";
        $wpdb->query($sql);
    }

    $table_name = $wpdb->prefix.'parser_soccervista_latest';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, country VARCHAR(100),  date VARCHAR(100), round VARCHAR(20), status VARCHAR(100), home VARCHAR(100), scope VARCHAR(100), await VARCHAR(100), PRIMARY KEY id(id))";
        $wpdb->query($sql);
    }

    $table_name = $wpdb->prefix.'parser_soccervista_last5';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, country VARCHAR(100),  rank VARCHAR(100), name VARCHAR(20), game VARCHAR(100), win VARCHAR(100), draw VARCHAR(100), los VARCHAR(100),
        goals VARCHAR(100), points VARCHAR(100), PRIMARY KEY id(id))";
        $wpdb->query($sql);
    }

    $table_name = $wpdb->prefix.'parser_soccervista_last5home';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, country VARCHAR(100),  rank VARCHAR(100), name VARCHAR(20), game VARCHAR(100), win VARCHAR(100), draw VARCHAR(100), los VARCHAR(100),
        goals VARCHAR(100), points VARCHAR(100), PRIMARY KEY id(id))";
        $wpdb->query($sql);
    }

    $table_name = $wpdb->prefix.'parser_soccervista_last5away';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, country VARCHAR(100),  rank VARCHAR(100), name VARCHAR(20), game VARCHAR(100), win VARCHAR(100), draw VARCHAR(100), los VARCHAR(100),
        goals VARCHAR(100), points VARCHAR(100), PRIMARY KEY id(id))";
        $wpdb->query($sql);
    }

    $table_name = $wpdb->prefix.'parser_soccervista_statistic';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $sql = "CREATE TABLE " . $table_name ." (id int (10) AUTO_INCREMENT, country VARCHAR(100),  rank VARCHAR(100), name VARCHAR(20), over15 VARCHAR(100), over25 VARCHAR(100), over35 VARCHAR(100), averegehome VARCHAR(100),
        averegeaway VARCHAR(100), averegetotal VARCHAR(100), PRIMARY KEY id(id))";
        $wpdb->query($sql);
    }
}

function parser_soccervista_unset_table($table_name = 'all'){
    global $wpdb;
    if ($table_name != 'all') {
        $table_name = $wpdb->prefix . $table_name;
        $sql = "TRUNCATE TABLE " . $table_name;
        $wpdb->query($sql);
    }
    else {
        $tables = array(
            'parser_soccervista_country',
            'parser_soccervista_main_table',
            'parser_soccervista_prediction1',
            'parser_soccervista_prediction2',
            'parser_soccervista_big',
            'parser_soccervista_homonly',
            'parser_soccervista_away',
            'parser_soccervista_latest',
            'parser_soccervista_last5',
            'parser_soccervista_last5home',
            'parser_soccervista_last5away',
            'parser_soccervista_statistic',
            );
        foreach ($tables as $table){
            $sql = "TRUNCATE TABLE " . $wpdb->prefix . $table;
            $wpdb->query($sql);
        }
    }
}

function parser_soccervista_add_to_db_country_table(){
    global $wpdb;
    $country_table = $wpdb->prefix.'parser_soccervista_country';
    $sql = "TRUNCATE TABLE ".$country_table;
    $wpdb->query($sql);
    $contrys = parse_soccervista_links();
    foreach ($contrys as $contry){
        $wpdb->insert($wpdb->prefix.'parser_soccervista_country', array('name' => trim($contry['name']), 'link' => $contry['link']));
    }
}


function parser_soccervista_add_to_db_main_table(){
    global $wpdb;

    $main_table = $wpdb->prefix.'parser_soccervista_main_table';

    $rows = parse_soccervista_main_page();

    foreach ($rows as $row){
       // $id_country = $wpdb->get_var($wpdb->prepare("SELECT id FROM $country_table WHERE name = %s", $row['country']));

        $wpdb->insert($main_table, array(
            'country' => trim($row['country']),
            'liga' => trim($row['liga']),
            'time' => $row['time'],
            'pic1' => implode(",", $row['pic1']),
            'pic2' => implode(",", $row['pic2']),
            'home' => $row['home'],
            'away' => $row['away'],
            'scope' => $row['scope'],
            'odds1' => $row['odds1'],
            'odds2' => $row['odds2'],
            'odds3' => $row['odds3'],
            'tips1' => $row['tips1'],
            'tips2' => $row['tips2'],
            'tips3' => $row['tips3'],
            ));
    }


}

function parser_soccervista_add_to_db_country_page(){
    global $wpdb;
    include 'config.php';
    parser_soccervista_unset_table('parser_soccervista_prediction1');
    $countrys = parse_soccervista_links();
    foreach ($countrys as $val){

        $inter = preg_match('/International/', $val['link']);
        
        if($inter == 0) {
            $tables = parse_soccervista_pages($val['link']);
            $country = $val['name'];

            foreach ($tables as $name => $table) {
                if (!empty($table)) {
                    switch ($name) {
                        case 'prediction1':
                            foreach ($table as $row) {
                                if (!empty($row)) {

                                    $wpdb->insert($wpdb->prefix . 'parser_soccervista_prediction1', array(
                                        'date' => trim($row['date']),
                                        'round' => trim($row['round']),
                                        'rait2' => $row['rait2'],
                                        'pic1' => implode(",", $row['pic1']),
                                        'pic2' => implode(",", $row['pic2']),
                                        'home' => $row['home'],
                                        'away' => $row['away'],
                                        'rait1' => $row['rait1'],
                                        'respre' => $row['respre'],
                                        'goals' => $row['goals'],
                                        'raitx' => $row['raitx'],
                                        'tips' => $row['tips'],
                                        'country' => $country,
                                    ));
                                } else {
                                    $f = fopen('error.log', 'a+');
                                    fwrite($f, $val['link'] . ' prediction1');
                                    fclose($f);
                                }
                            }
                            break;
                        case 'prediction2':
                            foreach ($table as $row) {
                                if (!empty($row)) {
                                    $wpdb->insert($wpdb->prefix . 'parser_soccervista_prediction2', array(
                                        'date' => trim($row['date']),
                                        'round' => trim($row['round']),
                                        'pic1' => implode(",", $row['pic1']),
                                        'pic2' => implode(",", $row['pic2']),
                                        'home' => $row['home'],
                                        'away' => $row['away'],
                                        'respre' => $row['respre'],
                                        'goals' => $row['goals'],
                                        'tips' => $row['tips'],
                                        'country' => $country,
                                    ));
                                } else {
                                    $f = fopen('error.log', 'a+');
                                    fwrite($f, $val['link'] . ' prediction2');
                                    fclose($f);
                                }
                            }
                            break;
                        case 'big':
                            foreach ($table as $row) {
                                if (!empty($row)) {
                                    $wpdb->insert($wpdb->prefix . 'parser_soccervista_big', array(
                                        'rank' => trim($row['rank']),
                                        'name' => trim($row['name']),
                                        'game' => $row['game'],
                                        'win' => $row['win'],
                                        'draw' => $row['draw'],
                                        'los' => $row['los'],
                                        'goals' => $row['gols'],
                                        'points' => $row['points'],
                                        'country' => $country,
                                    ));
                                } else {
                                    $f = fopen('error.log', 'a+');
                                    fwrite($f, $val['link'] . ' $big');
                                    fclose($f);
                                }
                            }
                            break;
                        case 'homonly':
                            foreach ($table as $row) {
                                if (!empty($row)) {
                                    $wpdb->insert($wpdb->prefix . 'parser_soccervista_homonly', array(
                                        'rank' => trim($row['rank']),
                                        'name' => trim($row['name']),
                                        'game' => $row['game'],
                                        'win' => $row['win'],
                                        'draw' => $row['draw'],
                                        'los' => $row['los'],
                                        'goals' => $row['gols'],
                                        'points' => $row['points'],
                                        'country' => $country,
                                    ));
                                } else {
                                    $f = fopen('error.log', 'a+');
                                    fwrite($f, $val['link'] . ' homonly');
                                    fclose($f);
                                }
                            }
                            break;
                        case 'last5away':
                            foreach ($table as $row) {
                                if (!empty($row)) {

                                    $wpdb->insert($wpdb->prefix . 'parser_soccervista_last5away', array(
                                        'rank' => trim($row['rank']),
                                        'name' => trim($row['name']),
                                        'game' => $row['game'],
                                        'win' => $row['win'],
                                        'draw' => $row['draw'],
                                        'los' => $row['los'],
                                        'goals' => $row['gols'],
                                        'points' => $row['points'],
                                        'country' => $country,
                                    ));
                                } else {
                                    $f = fopen('error.log', 'a+');
                                    fwrite($f, $val['link'] . ' last5away');
                                    fclose($f);
                                }
                            }
                            break;
                        case 'last5home':
                            foreach ($table as $row) {
                                if (!empty($row)) {

                                    $wpdb->insert($wpdb->prefix . 'parser_soccervista_last5home', array(
                                        'rank' => trim($row['rank']),
                                        'name' => trim($row['name']),
                                        'game' => $row['game'],
                                        'win' => $row['win'],
                                        'draw' => $row['draw'],
                                        'los' => $row['los'],
                                        'goals' => $row['gols'],
                                        'points' => $row['points'],
                                        'country' => $country,
                                    ));
                                } else {
                                    $f = fopen('error.log', 'a+');
                                    fwrite($f, $val['link'] . ' last5home');
                                    fclose($f);
                                }
                            }
                            break;
                        case 'last5':
                            foreach ($table as $row) {

                                if (!empty($row)) {
                                    $wpdb->insert($wpdb->prefix . 'parser_soccervista_last5', array(
                                        'rank' => trim($row['rank']),
                                        'name' => trim($row['name']),
                                        'game' => $row['game'],
                                        'win' => $row['win'],
                                        'draw' => $row['draw'],
                                        'los' => $row['los'],
                                        'goals' => $row['gols'],
                                        'points' => $row['points'],
                                        'country' => $country,
                                    ));
                                } else {
                                    $f = fopen('error.log', 'a+');
                                    fwrite($f, $val['link'] . ' last5');
                                    fclose($f);
                                }
                            }
                            break;
                        case 'latest':
                            foreach ($table as $row) {

                                if (!empty($row)) {

                                    $wpdb->insert($wpdb->prefix . 'parser_soccervista_latest', array(
                                        'date' => $row['date'],
                                        'round' => $row['round'],
                                        'status' => $row['status'],
                                        'home' => $row['home'],
                                        'scope' => $row['scope'],
                                        'await' => $row['await'],
                                        'country' => $country,
                                    ));
                                } else {
                                    $f = fopen('error.log', 'a+');
                                    fwrite($f, $val['link'] . ' latest');
                                    fclose($f);
                                }
                            }
                            break;
                        case 'away':
                            foreach ($table as $row) {
                                if (!empty($row)) {

                                    $wpdb->insert($wpdb->prefix . 'parser_soccervista_away', array(
                                        'rank' => trim($row['rank']),
                                        'name' => trim($row['name']),
                                        'game' => $row['game'],
                                        'win' => $row['win'],
                                        'draw' => $row['draw'],
                                        'los' => $row['los'],
                                        'goals' => $row['gols'],
                                        'points' => $row['points'],
                                        'country' => $country,
                                    ));
                                } else {
                                    $f = fopen('error.log', 'a+');
                                    fwrite($f, $val['link'] . ' away');
                                    fclose($f);
                                }
                            }
                            break;
                        case 'statistic':

                            foreach ($table as $row) {
                                if (!empty($row)) {

                                    $wpdb->insert($wpdb->prefix . 'parser_soccervista_statistic', array(
                                        'rank' => trim($row['rank']),
                                        'name' => trim($row['name']),
                                        'over15' => $row['over15'],
                                        'over25' => $row['over25'],
                                        'over35' => $row['over35'],
                                        'averegehome' => $row['averegehome'],
                                        'averegeaway' => $row['averegeaway'],
                                        'averegetotal' => $row['averegetotal'],
                                        'country' => $country,
                                    ));
                                } else {
                                    $f = fopen('error.log', 'a+');
                                    fwrite($f, $val['link'] . ' statistic');
                                    fclose($f);
                                }
                            }
                            break;
                    }
                } else {
                    $f = fopen('error.log', 'a+');
                    fwrite($f, $val['link']);
                    fclose($f);
                }


            }
        }
    }





}

function parse_soccervista_setting(){
    wp_enqueue_style( 'parser_soccervista_stylesheet', plugins_url('mainandcalendarv10.css', __FILE__) );
    echo "<h1 style='color: #ffa500'>Parser Setting</h1>";
    if (!empty($_POST['Crawl'])){
        parser_soccervista_unset_table();
        parser_soccervista_add_to_db_country_table();
        parser_soccervista_add_to_db_main_table();
        parser_soccervista_add_to_db_country_page();
        echo 'Data add in DB';
    }
    elseif (!empty($_POST['update'])) {
        global $wpdb;
        $update = $wpdb->update($_POST['table'],
            array(
                'country' => $_POST['country'],
                'liga' => $_POST['liga'],
                'time' => $_POST['time'],
                'pic1' => $_POST['pic1'],
                'home' => $_POST['home'],
                'pic2' => $_POST['pic2'],
                'away' => $_POST['away'],
                'scope' => $_POST['scope'],
                'odds1' => $_POST['odds1'],
                'odds2' => $_POST['odds2'],
                'odds3' => $_POST['odds3'],
                'tips1' => $_POST['tips1'],
                'tips2' => $_POST['tips2'],
                'tips3' => $_POST['tips3'],
            ),
            array( 'ID' => $_POST['id'] )
        );
        if ($update != false){
            echo 'Update row';
        }
        else{
            echo 'Error!';
        }
    }
    else{
        global $wpdb;

        if (!empty($_GET['id'])){
            switch ($_GET['table']){
                case '1':
                    $table = $wpdb->prefix . 'parser_soccervista_main_table';
            }
            $query = 'SELECT * FROM ' . $table . ' WHERE id =' .$_GET['id'];
            $res = $wpdb->get_row($query);
            echo "
            <form action='' method='post'>
            Country:<input type='text' value='". $res->country ."' name='country'><br/>
            Liga: <input type='text' value='". $res->liga ."' name='liga'><br/>
            Time: <input type='text' value='". $res->time ."' name='time'><br/>
            Pic1: <input type='text' value='". $res->pic1 ."' name='pic1'><br/>
            Home: <input type='text' value='". $res->home ."' name='home'><br/>
            Pic2: <input type='text' value='". $res->pic1 ."' name='pic2'><br/>
            Away: <input type='text' value='". $res->away ."' name='away'><br/>
            Score: <input type='text' value='". $res->scope ."' name='scope'><br/>
            Odds1: <input type='text' value='". $res->odds1 ."' name='odds1'><br/>
            Odds2: <input type='text' value='". $res->odds2 ."' name='odds2'><br/>
            Odds3: <input type='text' value='". $res->odds3 ."' name='odds3'><br/>
            Tips1: <input type='text' value='". $res->tips1 ."' name='tips1'><br/>
            Tips2: <input type='text' value='". $res->tips2 ."' name='tips2'><br/>
            Tips3: <input type='text' value='". $res->tips3 ."' name='tips3'><br/>
            <input type='hidden' value='update' name='update'>
            <input type='hidden' value='".$table."' name='table'>
            <input type='hidden' value='".$_GET['id']."' name='id'>
            <button>Edit</button>
            </form>
            ";
        }
        else{
            echo "
          <form action='/wp-admin/admin.php?page=parser-options' method='post' id='parser_form'>
          <input type='hidden' name='Crawl' value='Crawl'>
          <div style='font-size: 12pt; color: #ffa500'>Begin parsing: <button>Start</button></div><br/>
          </form>";
            parser_soccervista_show_main_taible();
        }

    }
}

function parser_soccervista_explode_pic($pics){
    $pic_array = explode(',', $pics);
    $pic = '';
    foreach ($pic_array as $val) {
        $pic .= '<span class="' . $val . '"></span>';
    }
    return $pic;
}

function parser_soccervista_show_tables($country){
    global $wpdb;
    $query = 'SELECT * FROM '. $wpdb->prefix . 'parser_soccervista_prediction1 WHERE country =\''.$country.'\'';
    $rows = $wpdb->get_results($query);
    if (count($rows)>0){
        echo '<h2 style="color: #ffa500">Prediction1</h2>';
        echo '<table class="upcoming">
        <thead><tr class="headupc"><td width="40">date</td><td width="35">round</td><td>home team</td><td>away team</td><td width="35">1</td><td width="35">X</td><td width="55">2</td><td>result prediction</td><td width="40"># goals</td><td width="30">tip</td><td width="14">&nbsp;</td><td width="32">edit</td></tr></thead>
        <tbody>';
        foreach ($rows as $row){

            $pic1 = parser_soccervista_explode_pic($row->pic1);
            $pic2 = parser_soccervista_explode_pic($row->pic2);

           echo '<tr class="predict"><td>'.$row->date.'</td><td align="center">'.$row->round.'</td><td><a href="/">'.$row->home.'</a><br>'.$pic1.'</td><td><a href="/">'.$row->away.'</a><br>'.$pic2.'</td><td><a href="/" rel="nofollow"><font color="#FFFFAA">'.$row->rait1.'</font></a></td><td><a href="/" rel="nofollow">'.$row->raitx.'</a></td><td><a href="/" rel="nofollow">'.$row->rait2.'</a></td><td><i>'.$row->respre.'</i></td><td>'.$row->goals.'</td><td>'.$row->tips.'</td><td><a href="/"><span class="wdl detail3"></span></a></td><td><a href="/wp-admin/admin.php?page=parser-options-countrys&table=2&id='.$row->id.'">edit</a></td></tr>';
        }
        echo ' </tbody>
        </table>';
    }
    $query = 'SELECT * FROM '. $wpdb->prefix . 'parser_soccervista_prediction2 WHERE country =\''.$country.'\'';
    $rows = $wpdb->get_results($query);
    if (count($rows)>0){
        echo '<h2 style="color: #ffa500">Prediction2</h2>';
        echo '<table class="upcoming">
        <thead><tbody>';
        foreach ($rows as $row){

            $pic1 = parser_soccervista_explode_pic($row->pic1);
            $pic2 = parser_soccervista_explode_pic($row->pic2);

            echo '<tr class="predict"><td width="40">'.$row->date.'</td><td>'.$row->round.'</td><td width="70">'.$pic1.'</td><td><a href="/">'.$row->home.'</a></td><td width="70">'.$pic2.'</td><td><a href="/">'.$row->away.'</a></td><td><i><font color="#FFFF00">'.$row->respre.'</font></i></td><td width="40">'.$row->tips.'</td><td width="35"></td><td><a href="/"><span class="wdl detail3"></span></a></td><td><a href="/wp-admin/admin.php?page=parser-options-countrys&table=3&id='.$row->id.'">edit</a></td></tr>';
        }
        echo ' </tbody>
        </table>';
    }

    $query = 'SELECT * FROM '. $wpdb->prefix . 'parser_soccervista_big WHERE country =\''.$country.'\'';
    $rows = $wpdb->get_results($query);
    if (count($rows)>0){
        echo '<h2 style="color: #ffa500">Main</h2>';
        echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td><td width="32">edit</td></tr></thead><tbody>';
        foreach ($rows as $row){
            echo '<tr class="onem"><td class="championsleague">'.$row->rank.'</td><td><a href="/">'.$row->name.'</a></td><td>'.$row->game.'</td><td>'.$row->win.'</td><td>'.$row->draw.'</td><td>'.$row->los.'</td><td>'.$row->goals.'</td><td><strong>'.$row->points.'</strong></td><td><a href="/wp-admin/admin.php?page=parser-options-countrys&table=4&id='.$row->id.'">edit</a></td></tr>';
        }
        echo ' </tbody>
        </table>';
    }

    $query = 'SELECT * FROM '. $wpdb->prefix . 'parser_soccervista_homonly WHERE country =\''.$country.'\'';
    $rows = $wpdb->get_results($query);
    if (count($rows)>0){
        echo '<h2 style="color: #ffa500">Home</h2>';
        echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td><td width="32">edit</td></tr></thead><tbody>';
        foreach ($rows as $row){
            echo '<tr class="onem"><td class="championsleague">'.$row->rank.'</td><td><a href="/">'.$row->name.'</a></td><td>'.$row->game.'</td><td>'.$row->win.'</td><td>'.$row->draw.'</td><td>'.$row->los.'</td><td>'.$row->goals.'</td><td><strong>'.$row->points.'</strong></td><td><a href="/wp-admin/admin.php?page=parser-options-countrys&table=5&id='.$row->id.'">edit</a></td></tr>';
        }
        echo ' </tbody>
        </table>';
    }

    $query = 'SELECT * FROM '. $wpdb->prefix . 'parser_soccervista_away WHERE country =\''.$country.'\'';
    $rows = $wpdb->get_results($query);
    if (count($rows)>0){
        echo '<h2 style="color: #ffa500">Away</h2>';
        echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td><td width="32">edit</td></tr></thead><tbody>';
        foreach ($rows as $row){
            echo '<tr class="onem"><td class="championsleague">'.$row->rank.'</td><td><a href="/">'.$row->name.'</a></td><td>'.$row->game.'</td><td>'.$row->win.'</td><td>'.$row->draw.'</td><td>'.$row->los.'</td><td>'.$row->goals.'</td><td><strong>'.$row->points.'</strong></td><td><a href="/wp-admin/admin.php?page=parser-options-countrys&table=6&id='.$row->id.'">edit</a></td></tr>';
        }
        echo ' </tbody>
        </table>';
    }

    $query = 'SELECT * FROM '. $wpdb->prefix . 'parser_soccervista_latest WHERE country =\''.$country.'\'';
    $rows = $wpdb->get_results($query);
    if (count($rows)>0){
        echo '<h2 style="color: #ffa500">Latest</h2>';
        echo '<table class="upcoming"><thead><tr class="headupc"><td width="45">date</td><td width="40">round</td><td>&nbsp;</td><td align="right" width="270">home team</td><td width="70">&nbsp;</td><td>away team</td><td width="14"></td><td width="32">edit</td></tr></thead><tbody>';
        foreach ($rows as $row){
            echo '<tr class="predict"><td>'.$row->date.'</td><td>'.$row->round.'</td><td>'.$row->status.'</td><td align="right" class="one">'.$row->home.'</td><td align="center">'.$row->scope.'</td><td class="one"><font color="#FFFF00">'.$row->await.'</font></td><td><a href="/"></a></td><td><a href="/wp-admin/admin.php?page=parser-options-countrys&table=7&id='.$row->id.'">edit</a></td></tr>';
        }
        echo ' </tbody>
        </table>';

    }

    $query = 'SELECT * FROM '. $wpdb->prefix . 'parser_soccervista_last5 WHERE country =\''.$country.'\'';
    $rows = $wpdb->get_results($query);
    if (count($rows)>0){
        echo '<h2 style="color: #ffa500">Last5</h2>';
        echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td><td width="32">edit</td></tr></thead><tbody>';
        foreach ($rows as $row){
            echo '<tr class="onem"><td class="championsleague">'.$row->rank.'</td><td><a href="/">'.$row->name.'</a></td><td>'.$row->game.'</td><td>'.$row->win.'</td><td>'.$row->draw.'</td><td>'.$row->los.'</td><td>'.$row->goals.'</td><td><strong>'.$row->points.'</strong></td><td><a href="/wp-admin/admin.php?page=parser-options-countrys&table=8&id='.$row->id.'">edit</a></td></tr>';
        }
        echo ' </tbody>
        </table>';
    }

    $query = 'SELECT * FROM '. $wpdb->prefix . 'parser_soccervista_last5home WHERE country =\''.$country.'\'';
    $rows = $wpdb->get_results($query);
    if (count($rows)>0){
        echo '<h2 style="color: #ffa500">Last5Home</h2>';
        echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td><td width="32">edit</td></tr></thead><tbody>';
        foreach ($rows as $row){
            echo '<tr class="onem"><td class="championsleague">'.$row->rank.'</td><td><a href="/">'.$row->name.'</a></td><td>'.$row->game.'</td><td>'.$row->win.'</td><td>'.$row->draw.'</td><td>'.$row->los.'</td><td>'.$row->goals.'</td><td><strong>'.$row->points.'</strong></td><td><a href="/wp-admin/admin.php?page=parser-options-countrys&table=9&id='.$row->id.'">edit</a></td></tr>';
        }
        echo ' </tbody>
        </table>';
    }

    $query = 'SELECT * FROM '. $wpdb->prefix . 'parser_soccervista_last5away WHERE country =\''.$country.'\'';
    $rows = $wpdb->get_results($query);
    if (count($rows)>0){
        echo '<h2 style="color: #ffa500">Last5Away</h2>';
        echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td><td width="32">edit</td></tr></thead><tbody>';
        foreach ($rows as $row){
            echo '<tr class="onem"><td class="championsleague">'.$row->rank.'</td><td><a href="/">'.$row->name.'</a></td><td>'.$row->game.'</td><td>'.$row->win.'</td><td>'.$row->draw.'</td><td>'.$row->los.'</td><td>'.$row->goals.'</td><td><strong>'.$row->points.'</strong></td><td><a href="/wp-admin/admin.php?page=parser-options-countrys&table=10&id='.$row->id.'">edit</a></td></tr>';
        }
        echo ' </tbody>
        </table>';
    }

    $query = 'SELECT * FROM '. $wpdb->prefix . 'parser_soccervista_statistic WHERE country =\''.$country.'\'';
    $rows = $wpdb->get_results($query);
    if (count($rows)>0){
        echo '<h2 style="color: #ffa500">Statistic</h2>';
        echo '<table class="all"><thead><tr class="headupc"><td colspan="2">&nbsp;</td><td colspan="3">Over in % of games</td><td colspan="3">Average # of goals</td></tr></thead><tbody><tr class="headupc"><td colspan="2">&nbsp;</td><td>1.5</td><td>2.5</td><td>3.5</td><td>Home</td><td>Away</td><td>Total</td><td width="32">edit</td></tr>';
        foreach ($rows as $row){
            echo '<tr class="onem"><td class="rank">'.$row->rank.'</td><td>'.$row->name.'</td><td class="over">'.$row->over15.'</td><td class="over">'.$row->over25.'</td><td class="under">'.$row->over35.'</td><td>'.$row->averegehome.'</td><td>'.$row->averegeaway.'</td><td>'.$row->averegetotal.'</td><td><a href="/wp-admin/admin.php?page=parser-options-countrys&table=11&id='.$row->id.'">edit</a></td></tr>';
        }
        echo ' </tbody>
        </table>';
    }

}

function parser_soccervista_show_main_taible(){

    global $wpdb;
    $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_main_table';
    $rows = $wpdb->get_results($query);
    $res = array();
    foreach ($rows as $row){
        $res[$row->country][$row->liga][]= $row;
    }

    echo '<table class="main"><tbody><tr class="headupa"><td colspan="6" bgcolor="#FFFFFF">&nbsp;</td><td class="head">&nbsp;</td><td>odds</td><td class="head2">&nbsp;</td><td class="head">&nbsp;</td><td>tips</td><td class="head2">&nbsp;</td></tr><tr class="headupa"><td width="18">&nbsp;</td><td width="83">&nbsp;</td><td>&nbsp;</td><td width="30"></td><td>&nbsp;</td><td width="83">&nbsp;</td><td width="32">&nbsp;1</td><td width="32">X</td><td width="32">2</td><td width="32" align="center">1X2</td><td width="32">goals</td><td width="32">score</td><td width="32">edit</td></tr>';
    foreach ($res as $country => $liga){
        foreach ($liga as $key => $matchs) {
            echo '<tr class="headupe"><td><span class="flag '.$country.'"></span></td><td>'.$country.'</td><td colspan="10"><a href="">'.$key.'</a></td></tr>';
            foreach ($matchs as $match) {
                $pics = explode(',', $match->pic1);

                $pic1 = '';
                $pic2 = '';
                foreach ($pics as $pic) {
                    $pic1 .= '<span class="' . $pic . '"></span>';
                }
                $pics = explode(',', $match->pic2);
                foreach ($pics as $pic) {
                    $pic2 .= '<span class="' . $pic . '"></span>';
                }
                echo '<tr class="onem"><td>' . $match->time . '</td><td class="away">' . $pic1 . '</td><td class="home">' . $match->home . '</td><td class="detail"><a href="/"><strong>' . $match->scope . '</strong></a></td><td class="away">' . $match->away . '</td><td>' . $pic2 . '</td><td>' . $match->odds1 . '</td><td>' . $match->odds2 . '</td><td>' . $match->odds3 . '</td><td align="center"><strong>' . $match->tips1 . '</strong></td><td align="center">' . $match->tips2 . '</td><td align="center">' . $match->tips3 . '</td><td><a href="/wp-admin/admin.php?page=parser-options&table=1&id='.$match->id.'">edit</a></td></tr>';
            }
        }
    }
    echo '</tbody></table>';
}

function parse_soccervista_setting_countrys(){
    wp_enqueue_style( 'parser_soccervista_stylesheet', plugins_url('mainandcalendarv10.css', __FILE__) );
    global $wpdb;
    if (!empty($_POST['update'])){
        global $wpdb;
        $query_array = array();
        foreach ($_POST as $key => $value){
            if ($key != 'update' && $key != 'id' && $key != 'table' ){
                $query_array[$key] = $value;
            }
        }
        $update = $wpdb->update($_POST['table'],
            $query_array,
            array( 'ID' => $_POST['id'] )
        );
        if ($update != false){
            echo 'Update row';
        }
        else{
            echo 'Error!';
        }
    }
    else {

        if (!empty($_GET['id'])) {
            switch ($_GET['table']) {
                case '2':
                    $table = $wpdb->prefix . 'parser_soccervista_prediction1';
                    $query = 'SELECT * FROM ' . $table . ' WHERE id =' . $_GET['id'];
                    $res = $wpdb->get_row($query);
                    echo "
            <form action='' method='post' style='background-color: #ffffff'>
            Country:<input type='text' value='" . $res->country . "' name='country'><br/>
            Date: <input type='text' value='" . $res->date . "' name='date'><br/>
            Round: <input type='text' value='" . $res->round . "' name='round'><br/>
            Pic1: <input type='text' value='" . $res->pic1 . "' name='pic1'><br/>
            Home: <input type='text' value='" . $res->home . "' name='home'><br/>
            Pic2: <input type='text' value='" . $res->pic1 . "' name='pic2'><br/>
            Away: <input type='text' value='" . $res->away . "' name='away'><br/>
            Rait1: <input type='text' value='" . $res->rait1 . "' name='rait1'><br/>
            RaitX: <input type='text' value='" . $res->raitx . "' name='raitx'><br/>
            Rait2: <input type='text' value='" . $res->rait2 . "' name='rait2'><br/>
            Respre: <input type='text' value='" . $res->respre . "' name='odds3'><br/>
            Tips: <input type='text' value='" . $res->tips . "' name='tips'><br/>
            <input type='hidden' value='update' name='update'>
            <input type='hidden' value='" . $table . "' name='table'>
            <input type='hidden' value='" . $_GET['id'] . "' name='id'>
            <button>Edit</button>
            </form>                
                ";
                    break;
                case '3':
                    $table = $wpdb->prefix . 'parser_soccervista_prediction2';
                    $query = 'SELECT * FROM ' . $table . ' WHERE id =' . $_GET['id'];
                    $res = $wpdb->get_row($query);
                    echo "
            <form action='' method='post' style='background-color: #ffffff'>
            Country:<input type='text' value='" . $res->country . "' name='country'><br/>
            Date: <input type='text' value='" . $res->date . "' name='date'><br/>
            Round: <input type='text' value='" . $res->round . "' name='round'><br/>
            Pic1: <input type='text' value='" . $res->pic1 . "' name='pic1'><br/>
            Home: <input type='text' value='" . $res->home . "' name='home'><br/>
            Pic2: <input type='text' value='" . $res->pic1 . "' name='pic2'><br/>
            Away: <input type='text' value='" . $res->away . "' name='away'><br/>           
            Respre: <input type='text' value='" . $res->respre . "' name='odds3'><br/>
            Goals: <input type='text' value='" . $res->goals . "' name='goals'><br/>
            Tips: <input type='text' value='" . $res->tips . "' name='tips'><br/>
            <input type='hidden' value='update' name='update'>
            <input type='hidden' value='" . $table . "' name='table'>
            <input type='hidden' value='" . $_GET['id'] . "' name='id'>
            <button>Edit</button>
            </form>                
                ";
                    break;
                case '4':
                    $table = $wpdb->prefix . 'parser_soccervista_big';
                    $query = 'SELECT * FROM ' . $table . ' WHERE id =' . $_GET['id'];
                    $res = $wpdb->get_row($query);
                    echo "
            <form action='' method='post' style='background-color: #ffffff'>
            Country:<input type='text' value='" . $res->country . "' name='country'><br/>
            Rank: <input type='text' value='" . $res->rank . "' name='rank'><br/>
            Name: <input type='text' value='" . $res->name . "' name='name'><br/>
            Game: <input type='text' value='" . $res->game . "' name='game'><br/>
            Win: <input type='text' value='" . $res->win . "' name='win'><br/>
            Draw: <input type='text' value='" . $res->draw . "' name='draw'><br/>
            Lose: <input type='text' value='" . $res->los . "' name='los'><br/>           
            Points: <input type='text' value='" . $res->points . "' name='points'><br/>
            <input type='hidden' value='update' name='update'>
            <input type='hidden' value='" . $table . "' name='table'>
            <input type='hidden' value='" . $_GET['id'] . "' name='id'>
            <button>Edit</button>
            </form>                
                ";
                    break;
                case '5':
                    $table = $wpdb->prefix . 'parser_soccervista_homonly';
                    $query = 'SELECT * FROM ' . $table . ' WHERE id =' . $_GET['id'];
                    $res = $wpdb->get_row($query);
                    echo "
            <form action='' method='post' style='background-color: #ffffff'>
            Country:<input type='text' value='" . $res->country . "' name='country'><br/>
            Rank: <input type='text' value='" . $res->rank . "' name='rank'><br/>
            Name: <input type='text' value='" . $res->name . "' name='name'><br/>
            Game: <input type='text' value='" . $res->game . "' name='game'><br/>
            Win: <input type='text' value='" . $res->win . "' name='win'><br/>
            Draw: <input type='text' value='" . $res->draw . "' name='draw'><br/>
            Lose: <input type='text' value='" . $res->los . "' name='los'><br/>           
            Points: <input type='text' value='" . $res->points . "' name='points'><br/>
            <input type='hidden' value='update' name='update'>
            <input type='hidden' value='" . $table . "' name='table'>
            <input type='hidden' value='" . $_GET['id'] . "' name='id'>
            <button>Edit</button>
            </form>                
                ";
                    break;
                case '6':
                    $table = $wpdb->prefix . 'parser_soccervista_away';
                    $query = 'SELECT * FROM ' . $table . ' WHERE id =' . $_GET['id'];
                    $res = $wpdb->get_row($query);
                    echo "
            <form action='' method='post' style='background-color: #ffffff'>
            Country:<input type='text' value='" . $res->country . "' name='country'><br/>
            Rank: <input type='text' value='" . $res->rank . "' name='rank'><br/>
            Name: <input type='text' value='" . $res->name . "' name='name'><br/>
            Game: <input type='text' value='" . $res->game . "' name='game'><br/>
            Win: <input type='text' value='" . $res->win . "' name='win'><br/>
            Draw: <input type='text' value='" . $res->draw . "' name='draw'><br/>
            Lose: <input type='text' value='" . $res->los . "' name='los'><br/>           
            Points: <input type='text' value='" . $res->points . "' name='points'><br/>
            <input type='hidden' value='update' name='update'>
            <input type='hidden' value='" . $table . "' name='table'>
            <input type='hidden' value='" . $_GET['id'] . "' name='id'>
            <button>Edit</button>
            </form>                
                ";
                    break;
                case '7':
                    $table = $wpdb->prefix . 'parser_soccervista_latest';
                    $query = 'SELECT * FROM ' . $table . ' WHERE id =' . $_GET['id'];
                    $res = $wpdb->get_row($query);
                    echo "
            <form action='' method='post' style='background-color: #ffffff'>
            Country:<input type='text' value='" . $res->country . "' name='country'><br/>
            Date: <input type='text' value='" . $res->date . "' name='date'><br/>
            Round: <input type='text' value='" . $res->round . "' name='round'><br/>
            Status: <input type='text' value='" . $res->status . "' name='status'><br/>
            Home: <input type='text' value='" . $res->home . "' name='home'><br/>
            Score: <input type='text' value='" . $res->scope . "' name='scope'><br/>
            Away: <input type='text' value='" . $res->await . "' name='await'><br/>           
            <input type='hidden' value='update' name='update'>
            <input type='hidden' value='" . $table . "' name='table'>
            <input type='hidden' value='" . $_GET['id'] . "' name='id'>
            <button>Edit</button>
            </form>                
                ";
                    break;
                case '8':
                    $table = $wpdb->prefix . 'parser_soccervista_last5';
                    $query = 'SELECT * FROM ' . $table . ' WHERE id =' . $_GET['id'];
                    $res = $wpdb->get_row($query);
                    echo "
            <form action='' method='post' style='background-color: #ffffff'>
            Country:<input type='text' value='" . $res->country . "' name='country'><br/>
            Rank: <input type='text' value='" . $res->rank . "' name='rank'><br/>
            Name: <input type='text' value='" . $res->name . "' name='name'><br/>
            Game: <input type='text' value='" . $res->game . "' name='game'><br/>
            Win: <input type='text' value='" . $res->win . "' name='win'><br/>
            Draw: <input type='text' value='" . $res->draw . "' name='draw'><br/>
            Lose: <input type='text' value='" . $res->los . "' name='los'><br/>           
            Points: <input type='text' value='" . $res->points . "' name='points'><br/>
            <input type='hidden' value='update' name='update'>
            <input type='hidden' value='" . $table . "' name='table'>
            <input type='hidden' value='" . $_GET['id'] . "' name='id'>
            <button>Edit</button>
            </form>                
                ";
                    break;
                case '9':
                    $table = $wpdb->prefix . 'parser_soccervista_last5home';
                    $query = 'SELECT * FROM ' . $table . ' WHERE id =' . $_GET['id'];
                    $res = $wpdb->get_row($query);
                    echo "
            <form action='' method='post' style='background-color: #ffffff'>
            Country:<input type='text' value='" . $res->country . "' name='country'><br/>
            Rank: <input type='text' value='" . $res->rank . "' name='rank'><br/>
            Name: <input type='text' value='" . $res->name . "' name='name'><br/>
            Game: <input type='text' value='" . $res->game . "' name='game'><br/>
            Win: <input type='text' value='" . $res->win . "' name='win'><br/>
            Draw: <input type='text' value='" . $res->draw . "' name='draw'><br/>
            Lose: <input type='text' value='" . $res->los . "' name='los'><br/>           
            Points: <input type='text' value='" . $res->points . "' name='points'><br/>
            <input type='hidden' value='update' name='update'>
            <input type='hidden' value='" . $table . "' name='table'>
            <input type='hidden' value='" . $_GET['id'] . "' name='id'>
            <button>Edit</button>
            </form>                
                ";
                    break;
                case '10':
                    $table = $wpdb->prefix . 'parser_soccervista_last5away';
                    $query = 'SELECT * FROM ' . $table . ' WHERE id =' . $_GET['id'];
                    $res = $wpdb->get_row($query);
                    echo "
            <form action='' method='post' style='background-color: #ffffff'>
            Country:<input type='text' value='" . $res->country . "' name='country'><br/>
            Rank: <input type='text' value='" . $res->rank . "' name='rank'><br/>
            Name: <input type='text' value='" . $res->name . "' name='name'><br/>
            Game: <input type='text' value='" . $res->game . "' name='game'><br/>
            Win: <input type='text' value='" . $res->win . "' name='win'><br/>
            Draw: <input type='text' value='" . $res->draw . "' name='draw'><br/>
            Lose: <input type='text' value='" . $res->los . "' name='los'><br/>           
            Points: <input type='text' value='" . $res->points . "' name='points'><br/>
            <input type='hidden' value='update' name='update'>
            <input type='hidden' value='" . $table . "' name='table'>
            <input type='hidden' value='" . $_GET['id'] . "' name='id'>
            <button>Edit</button>
            </form>                
                ";
                    break;
                case '11':
                    $table = $wpdb->prefix . 'parser_soccervista_statistic';
                    $query = 'SELECT * FROM ' . $table . ' WHERE id =' . $_GET['id'];
                    $res = $wpdb->get_row($query);
                    echo "
            <form action='' method='post' style='background-color: #ffffff'>
            Country:<input type='text' value='" . $res->country . "' name='country'><br/>
            Rank: <input type='text' value='" . $res->rank . "' name='rank'><br/>
            Name: <input type='text' value='" . $res->name . "' name='name'><br/>
            Over1.5: <input type='text' value='" . $res->over15 . "' name='over15'><br/>
            Over2.5: <input type='text' value='" . $res->over25 . "' name='over25'><br/>
            Over3.5: <input type='text' value='" . $res->over35 . "' name='over35'><br/>
            AveregeHome: <input type='text' value='" . $res->averegehome . "' name='averegehome'><br/>           
            AveregeAway: <input type='text' value='" . $res->averegeaway . "' name='averegeaway'><br/>
            AveregeTotal: <input type='text' value='" . $res->averegetotal . "' name='averegetotal'><br/>
            <input type='hidden' value='update' name='update'>
            <input type='hidden' value='" . $table . "' name='table'>
            <input type='hidden' value='" . $_GET['id'] . "' name='id'>
            <button>Edit</button>
            </form>                
                ";
                    break;


            }

        } else {
            if (empty($_GET['contry'])) {
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_country';
                $countrys = $wpdb->get_col($query, 1);
                echo '<h1 style="color: #ffa500">Countries:</h1><ol>';
                foreach ($countrys as $country) {
                    echo '<li style="color: #ffa500"><a style="color: #ffbf00; font-size: 12pt;" href="/wp-admin/admin.php?page=parser-options-countrys&contry=' . $country . '">' . $country . '</a> </li>';
                }
                echo '</ol>';
            } else {

                echo '<h1 style="color: #ffa500">'.$_GET['contry'].'</h1>';
                parser_soccervista_show_tables($_GET['contry']);
            }
        }
    }
}

function parser_soccervista_shortcode($attr){
    wp_enqueue_style( 'parser_soccervista_stylesheet', plugins_url('mainandcalendarv10.css', __FILE__) );
    global $wpdb;
    if (!empty($attr['country'])){
        $country = strtolower($attr['country']);
    }
    else{
        $country = '';
    }

    if (!empty($attr['table'])){
        $table = strtolower($attr['table']);
    }
    else{
        $table = '';
    }

    if (($table != '') && ($country != '')) {

        switch ($table) {
            case 'prediction1':
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_prediction1 WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    //echo '<h2 style="color: #ffa500">Prediction1</h2>';
                    echo '<table class="upcoming">
        <thead><tr class="headupc"><td width="40">date</td><td width="35">round</td><td>home team</td><td>away team</td><td width="35">1</td><td width="35">X</td><td width="55">2</td><td>result prediction</td><td width="40"># goals</td><td width="30">tip</td><td width="14">&nbsp;</td></tr></thead>
        <tbody>';
                    foreach ($rows as $row) {

                        $pic1 = parser_soccervista_explode_pic($row->pic1);
                        $pic2 = parser_soccervista_explode_pic($row->pic2);

                        echo '<tr class="predict"><td>' . $row->date . '</td><td align="center">' . $row->round . '</td><td><a href="/">' . $row->home . '</a><br>' . $pic1 . '</td><td><a href="/">' . $row->away . '</a><br>' . $pic2 . '</td><td><a href="/" rel="nofollow"><font color="#FFFFAA">' . $row->rait1 . '</font></a></td><td><a href="/" rel="nofollow">' . $row->raitx . '</a></td><td><a href="/" rel="nofollow">' . $row->rait2 . '</a></td><td><i>' . $row->respre . '</i></td><td>' . $row->goals . '</td><td>' . $row->tips . '</td><td><a href="/"><span class="wdl detail3"></span></a></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }
                break;
            case 'prediction2':
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_prediction2 WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    //echo '<h2 style="color: #ffa500">Prediction2</h2>';
                    echo '<table class="upcoming">
        <thead><tbody>';
                    foreach ($rows as $row) {

                        $pic1 = parser_soccervista_explode_pic($row->pic1);
                        $pic2 = parser_soccervista_explode_pic($row->pic2);

                        echo '<tr class="predict"><td width="40">' . $row->date . '</td><td>' . $row->round . '</td><td width="70">' . $pic1 . '</td><td><a href="/">' . $row->home . '</a></td><td width="70">' . $pic2 . '</td><td><a href="/">' . $row->away . '</a></td><td><i><font color="#FFFF00">' . $row->respre . '</font></i></td><td width="40">' . $row->tips . '</td><td width="35"></td><td><a href="/"><span class="wdl detail3"></span></a></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }
                break;
            case 'big':
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_big WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    //echo '<h2 style="color: #ffa500">Main</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }
                break;
            case 'homonly':
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_homonly WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    //echo '<h2 style="color: #ffa500">Home</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }
                break;
            case 'away':
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_away WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                   // echo '<h2 style="color: #ffa500">Away</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }
                break;
            case 'latest':
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_latest WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                   // echo '<h2 style="color: #ffa500">Latest</h2>';
                    echo '<table class="upcoming"><thead><tr class="headupc"><td width="45">date</td><td width="40">round</td><td>&nbsp;</td><td align="right" width="270">home team</td><td width="70">&nbsp;</td><td>away team</td><td width="14"></td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="predict"><td>' . $row->date . '</td><td>' . $row->round . '</td><td>' . $row->status . '</td><td align="right" class="one">' . $row->home . '</td><td align="center">' . $row->scope . '</td><td class="one"><font color="#FFFF00">' . $row->await . '</font></td><td><a href="/"></a></td></tr>';
                    }
                    echo ' </tbody>
        </table>';

                }
                break;
            case 'last5':
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_last5 WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                   // echo '<h2 style="color: #ffa500">Last5</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }
                break;
            case 'last5home':
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_last5home WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                   // echo '<h2 style="color: #ffa500">Last5Home</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }
                break;
            case 'last5away':
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_last5away WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                   // echo '<h2 style="color: #ffa500">Last5Away</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }
                break;
            case 'statistic':
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_statistic WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    //echo '<h2 style="color: #ffa500">Statistic</h2>';
                    echo '<table class="all"><thead><tr class="headupc"><td colspan="2">&nbsp;</td><td colspan="3">Over in % of games</td><td colspan="3">Average # of goals</td></tr></thead><tbody><tr class="headupc"><td colspan="2">&nbsp;</td><td>1.5</td><td>2.5</td><td>3.5</td><td>Home</td><td>Away</td><td>Total</td></tr>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="rank">' . $row->rank . '</td><td>' . $row->name . '</td><td class="over">' . $row->over15 . '</td><td class="over">' . $row->over25 . '</td><td class="under">' . $row->over35 . '</td><td>' . $row->averegehome . '</td><td>' . $row->averegeaway . '</td><td>' . $row->averegetotal . '</td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }
                break;
            case 'main':
                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_main_table';
                $rows = $wpdb->get_results($query);
                $res = array();
                foreach ($rows as $row) {
                    $res[$row->country][$row->liga][] = $row;
                }
                echo '<table class="main"><tbody><tr class="headupa"><td colspan="6" bgcolor="#FFFFFF">&nbsp;</td><td class="head">&nbsp;</td><td>odds</td><td class="head2">&nbsp;</td><td class="head">&nbsp;</td><td>tips</td><td class="head2">&nbsp;</td></tr><tr class="headupa"><td width="18">&nbsp;</td><td width="83">&nbsp;</td><td>&nbsp;</td><td width="30"></td><td>&nbsp;</td><td width="83">&nbsp;</td><td width="32">&nbsp;1</td><td width="32">X</td><td width="32">2</td><td width="32" align="center">1X2</td><td width="32">goals</td><td width="32">score</td></tr>';
                foreach ($res as $country => $liga) {
                    foreach ($liga as $key => $matchs) {
                        echo '<tr class="headupe"><td><span class="flag ' . $country . '"></span></td><td>' . $country . '</td><td colspan="10"><a href="">' . $key . '</a></td></tr>';
                        foreach ($matchs as $match) {
                            $pics = explode(',', $match->pic1);

                            $pic1 = '';
                            $pic2 = '';
                            foreach ($pics as $pic) {
                                $pic1 .= '<span class="' . $pic . '"></span>';
                            }
                            $pics = explode(',', $match->pic2);
                            foreach ($pics as $pic) {
                                $pic2 .= '<span class="' . $pic . '"></span>';
                            }
                            echo '<tr class="onem"><td>' . $match->time . '</td><td class="away">' . $pic1 . '</td><td class="home">' . $match->home . '</td><td class="detail"><a href="/"><strong>' . $match->scope . '</strong></a></td><td class="away">' . $match->away . '</td><td>' . $pic2 . '</td><td>' . $match->odds1 . '</td><td>' . $match->odds2 . '</td><td>' . $match->odds3 . '</td><td align="center"><strong>' . $match->tips1 . '</strong></td><td align="center">' . $match->tips2 . '</td><td align="center">' . $match->tips3 . '</td></tr>';
                        }
                    }
                }
                echo '</tbody></table>';
                break;
        }
    }
    elseif ($table == '' && $country != ''){
        $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_prediction1 WHERE country =\'' . $country . '\'';
        $rows = $wpdb->get_results($query);
        if (count($rows) > 0) {
            //echo '<h2 style="color: #ffa500">Prediction1</h2>';
            echo '<table class="upcoming">
        <thead><tr class="headupc"><td width="40">date</td><td width="35">round</td><td>home team</td><td>away team</td><td width="35">1</td><td width="35">X</td><td width="55">2</td><td>result prediction</td><td width="40"># goals</td><td width="30">tip</td><td width="14">&nbsp;</td></tr></thead>
        <tbody>';
            foreach ($rows as $row) {

                $pic1 = parser_soccervista_explode_pic($row->pic1);
                $pic2 = parser_soccervista_explode_pic($row->pic2);

                echo '<tr class="predict"><td>' . $row->date . '</td><td align="center">' . $row->round . '</td><td><a href="/">' . $row->home . '</a><br>' . $pic1 . '</td><td><a href="/">' . $row->away . '</a><br>' . $pic2 . '</td><td><a href="/" rel="nofollow"><font color="#FFFFAA">' . $row->rait1 . '</font></a></td><td><a href="/" rel="nofollow">' . $row->raitx . '</a></td><td><a href="/" rel="nofollow">' . $row->rait2 . '</a></td><td><i>' . $row->respre . '</i></td><td>' . $row->goals . '</td><td>' . $row->tips . '</td><td><a href="/"><span class="wdl detail3"></span></a></td></tr>';
            }
            echo ' </tbody>
        </table>';
        }

                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_prediction2 WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    //echo '<h2 style="color: #ffa500">Prediction2</h2>';
                    echo '<table class="upcoming">
        <thead><tbody>';
                    foreach ($rows as $row) {

                        $pic1 = parser_soccervista_explode_pic($row->pic1);
                        $pic2 = parser_soccervista_explode_pic($row->pic2);

                        echo '<tr class="predict"><td width="40">' . $row->date . '</td><td>' . $row->round . '</td><td width="70">' . $pic1 . '</td><td><a href="/">' . $row->home . '</a></td><td width="70">' . $pic2 . '</td><td><a href="/">' . $row->away . '</a></td><td><i><font color="#FFFF00">' . $row->respre . '</font></i></td><td width="40">' . $row->tips . '</td><td width="35"></td><td><a href="/"><span class="wdl detail3"></span></a></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }

                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_big WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    //echo '<h2 style="color: #ffa500">Main</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td><td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }

                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_homonly WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    //echo '<h2 style="color: #ffa500">Home</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }

                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_away WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    // echo '<h2 style="color: #ffa500">Away</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }

                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_latest WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    // echo '<h2 style="color: #ffa500">Latest</h2>';
                    echo '<table class="upcoming"><thead><tr class="headupc"><td width="45">date</td><td width="40">round</td><td>&nbsp;</td><td align="right" width="270">home team</td><td width="70">&nbsp;</td><td>away team</td><td width="14"></td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="predict"><td>' . $row->date . '</td><td>' . $row->round . '</td><td>' . $row->status . '</td><td align="right" class="one">' . $row->home . '</td><td align="center">' . $row->scope . '</td><td class="one"><font color="#FFFF00">' . $row->await . '</font></td><td><a href="/"></a></td></tr>';
                    }
                    echo ' </tbody>
        </table>';

                }

                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_last5 WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    // echo '<h2 style="color: #ffa500">Last5</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }

                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_last5home WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    // echo '<h2 style="color: #ffa500">Last5Home</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }

                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_last5away WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    // echo '<h2 style="color: #ffa500">Last5Away</h2>';
                    echo '<table class="all"><thead><tr class="head"><td width="40" class="head">#</td><td>team</td><td width="40">G</td><td width="40">W</td><td width="40">D</td><td width="40">L</td><td width="48">goals</td><td width="45" class="head2">points</td></tr></thead><tbody>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="championsleague">' . $row->rank . '</td><td><a href="/">' . $row->name . '</a></td><td>' . $row->game . '</td><td>' . $row->win . '</td><td>' . $row->draw . '</td><td>' . $row->los . '</td><td>' . $row->goals . '</td><td><strong>' . $row->points . '</strong></td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }

                $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_statistic WHERE country =\'' . $country . '\'';
                $rows = $wpdb->get_results($query);
                if (count($rows) > 0) {
                    //echo '<h2 style="color: #ffa500">Statistic</h2>';
                    echo '<table class="all"><thead><tr class="headupc"><td colspan="2">&nbsp;</td><td colspan="3">Over in % of games</td><td colspan="3">Average # of goals</td></tr></thead><tbody><tr class="headupc"><td colspan="2">&nbsp;</td><td>1.5</td><td>2.5</td><td>3.5</td><td>Home</td><td>Away</td><td>Total</td></tr>';
                    foreach ($rows as $row) {
                        echo '<tr class="onem"><td class="rank">' . $row->rank . '</td><td>' . $row->name . '</td><td class="over">' . $row->over15 . '</td><td class="over">' . $row->over25 . '</td><td class="under">' . $row->over35 . '</td><td>' . $row->averegehome . '</td><td>' . $row->averegeaway . '</td><td>' . $row->averegetotal . '</td></tr>';
                    }
                    echo ' </tbody>
        </table>';
                }

    }
    else {
        $query = 'SELECT * FROM ' . $wpdb->prefix . 'parser_soccervista_main_table';
        $rows = $wpdb->get_results($query);
        $res = array();
        foreach ($rows as $row) {
            $res[$row->country][$row->liga][] = $row;
        }
        echo '<table class="main"><tbody><tr class="headupa"><td colspan="6" bgcolor="#FFFFFF">&nbsp;</td><td class="head">&nbsp;</td><td>odds</td><td class="head2">&nbsp;</td><td class="head">&nbsp;</td><td>tips</td><td class="head2">&nbsp;</td></tr><tr class="headupa"><td width="18">&nbsp;</td><td width="83">&nbsp;</td><td>&nbsp;</td><td width="30"></td><td>&nbsp;</td><td width="83">&nbsp;</td><td width="32">&nbsp;1</td><td width="32">X</td><td width="32">2</td><td width="32" align="center">1X2</td><td width="32">goals</td><td width="32">score</td></tr>';
        foreach ($res as $country => $liga) {
            foreach ($liga as $key => $matchs) {
                echo '<tr class="headupe"><td><span class="flag ' . $country . '"></span></td><td>' . $country . '</td><td colspan="10"><a href="">' . $key . '</a></td></tr>';
                foreach ($matchs as $match) {
                    $pics = explode(',', $match->pic1);

                    $pic1 = '';
                    $pic2 = '';
                    foreach ($pics as $pic) {
                        $pic1 .= '<span class="' . $pic . '"></span>';
                    }
                    $pics = explode(',', $match->pic2);
                    foreach ($pics as $pic) {
                        $pic2 .= '<span class="' . $pic . '"></span>';
                    }
                    echo '<tr class="onem"><td>' . $match->time . '</td><td class="away">' . $pic1 . '</td><td class="home">' . $match->home . '</td><td class="detail"><a href="/"><strong>' . $match->scope . '</strong></a></td><td class="away">' . $match->away . '</td><td>' . $pic2 . '</td><td>' . $match->odds1 . '</td><td>' . $match->odds2 . '</td><td>' . $match->odds3 . '</td><td align="center"><strong>' . $match->tips1 . '</strong></td><td align="center">' . $match->tips2 . '</td><td align="center">' . $match->tips3 . '</td></tr>';
                }
            }
        }
        echo '</tbody></table>';
    }


}
