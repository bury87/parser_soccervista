<?php
/**
 * @package Parser Soccervista
 * @version 0.0.1
 */
/*
/*
Plugin Name: Parser Soccervista.com
Description: Parser pages from Soccervista.com.
Version: 0.0.1
Author: Shasha Burmey
*/

/*  Copyright 2016  Burmey  (email : sasha.burmey {at} gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//*/
//if (!defined("WPINC")) {
//    die;
//}
//if(is_admin()){
//    require_once(plugin_dir_path(__FILE__) . "functions.php");
//}
require_once(plugin_dir_path(__FILE__) . "functions.php");
register_activation_hook(__FILE__, 'parse_soccervista_activation');
register_deactivation_hook(__FILE__, 'parse_soccervista_deactivation');
add_action('admin_menu', function(){
    add_menu_page( 'Parser Soccervista.com', 'Parser setting', 'manage_options', 'parser-options', 'parse_soccervista_setting', '', 66 );
    add_submenu_page( 'parser-options', "Parser Soccervista.com", 'Countrys', 'manage_options', 'parser-options-countrys', 'parse_soccervista_setting_countrys');
} );
add_shortcode('parser','parser_soccervista_shortcode');


